package net.HolioCraft.HolioTools.updater;

import net.HolioCraft.HolioTools.main.Main;

import java.io.File;

import net.HolioCraft.HolioTools.updater.Updater.UpdateResult;
import net.HolioCraft.HolioTools.updater.Updater.UpdateType;

import org.bukkit.Bukkit;


public class UpdateChecker 
{
	private File file;
	private Main plugin;
	
	public UpdateChecker(Main plugin, File file)
	{
		this.plugin = plugin;
		this.file = file;
	}
	
	/**
	 * Launches the update checker for HolioTools
	 */
	public void startUpdater()
	{
		boolean Download = plugin.getConfig().getBoolean("Update.Download");
		boolean Check = plugin.getConfig().getBoolean("Update.Check");
		
		//If we are checking for updates and downloading if available.
		if(Check && Download)
		{
			final Updater updater = new Updater(plugin, 75051, file, UpdateType.DEFAULT, true);
			Bukkit.getScheduler().runTaskTimer(plugin, new Runnable() {
				public void run()
				{
					UpdateResult result = updater.getResult();
					switch(result)
					{
						case SUCCESS:
							plugin.logger.info("An update for HolioTools is available! Restart your server to update!");
							break;
						case NO_UPDATE:
							plugin.logger.info("HolioTools is up to date!");
							break;
						default:
							break;
					}
				}
			}, 400L, 72000L);
		}
		
		//If we are checking, but not downloading updates.
		else if(Check && !Download)
		{
			final Updater updater = new Updater(plugin, 75051, file, UpdateType.NO_DOWNLOAD, true);
			
			Bukkit.getScheduler().runTaskTimer(plugin, new Runnable() {
				public void run()
				{
					UpdateResult result = updater.getResult();
					switch(result)
					{
						case UPDATE_AVAILABLE:
							plugin.logger.info(String.format("There is an update available for HolioTools! (Version: %s)", updater.getLatestName()));
							plugin.logger.info(String.format("Download it here: %s",updater.getLatestFileLink()));
							break;
						case NO_UPDATE:
							plugin.logger.info("HolioTools is up to date!");
							break;
						default:
							break;
					}
				}
			}, 400L, 432000L);
		}
		
		//If update check is disabled
		else if(!Check)
		{
			Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
				public void run()
				{
					plugin.logger.info("HolioTools update checking is disabled. Skipping update check.");
				}
			}, 400L);
		}
	}	
}
