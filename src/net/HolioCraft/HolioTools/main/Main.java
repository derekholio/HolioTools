package net.HolioCraft.HolioTools.main;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import net.HolioCraft.HolioTools.commands.Clearall;
import net.HolioCraft.HolioTools.commands.Down;
import net.HolioCraft.HolioTools.commands.HolioTools;
import net.HolioCraft.HolioTools.commands.Leetspeak;
import net.HolioCraft.HolioTools.commands.MakeOP;
import net.HolioCraft.HolioTools.commands.MakeStaff;
import net.HolioCraft.HolioTools.commands.ODIN;
import net.HolioCraft.HolioTools.commands.RageQuit;
import net.HolioCraft.HolioTools.commands.Rtp;
import net.HolioCraft.HolioTools.commands.Spank;
import net.HolioCraft.HolioTools.commands.UUID;
import net.HolioCraft.HolioTools.commands.whatgm;
import net.HolioCraft.HolioTools.config.ConfigChecker;
import net.HolioCraft.HolioTools.events.PlayerChatListener;
import net.HolioCraft.HolioTools.events.PlayerDeathListener;
import net.HolioCraft.HolioTools.events.PlayerJoinListener;
import net.HolioCraft.HolioTools.events.PlayerPreLoginListener;
import net.HolioCraft.HolioTools.events.PlayerQuitListener;
import net.HolioCraft.HolioTools.metrics.Metrics;
import net.HolioCraft.HolioTools.updater.UpdateChecker;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;


public class Main extends JavaPlugin implements Listener
{
	public final Logger logger = Logger.getLogger("Minecraft");
	public static Main plugin;
	public List<Player> enabled_users = new ArrayList<Player>();//For leetspeak
	public List<Player> staff = new ArrayList<Player>();
	public List<Player> RQ = new ArrayList<Player>();
	
	public PluginDescriptionFile pdfFile;
	
	@Override
	public void onEnable()
	{
		plugin = this;
		pdfFile = plugin.getDescription();
		
		//Confirm that config.yml exists, and all parts are there.
		ConfigChecker cc = new ConfigChecker(plugin);
		cc.checkConfig();
		
		//Launches the update checker.
		UpdateChecker uc = new UpdateChecker(plugin, plugin.getFile());
		uc.startUpdater();
		
		//Set up command Executors
		getCommand("down").setExecutor(new Down(plugin));
		getCommand("clearall").setExecutor(new Clearall(plugin));
		getCommand("HolioTools").setExecutor(new HolioTools(plugin));
		getCommand("leetspeak").setExecutor(new Leetspeak(plugin));
		getCommand("makeop").setExecutor(new MakeOP(plugin));
		getCommand("makestaff").setExecutor(new MakeStaff(plugin));
		getCommand("odin").setExecutor(new ODIN(plugin));
		getCommand("ragequit").setExecutor(new RageQuit(plugin));
		getCommand("rtp").setExecutor(new Rtp(plugin));
		getCommand("spank").setExecutor(new Spank(plugin));
		getCommand("whatgm").setExecutor(new whatgm(plugin));
		getCommand("UUID").setExecutor(new UUID(plugin));
		//End of command executors
		
		//Register events
		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(plugin, plugin);
		pm.registerEvents(new PlayerDeathListener(plugin), plugin);
		pm.registerEvents(new PlayerChatListener(plugin), plugin);
		pm.registerEvents(new PlayerJoinListener(plugin), plugin);
		pm.registerEvents(new PlayerQuitListener(plugin), plugin);
		pm.registerEvents(new PlayerPreLoginListener(plugin), plugin);
		//End of events
		
		//Add staff to list
		Collection<? extends Player> p = Bukkit.getServer().getOnlinePlayers();
		//Player[] p = Bukkit.getServer().getOnlinePlayers();
		for(Player player : p)
		{
			if(player.hasPermission("holiotools.staff") || player.isOp())
			{
				staff.add(player);
			}
		}
		
		//Get Stats config
		boolean stats = plugin.getConfig().getBoolean("Stats");
		
		//Start stats if true.
		if(stats)
		{
			try
			{
				Metrics metrics = new Metrics(plugin);
				metrics.start();
			}
			catch(IOException e)
			{
				plugin.logger.info("Metrics failed to submit!");
				e.printStackTrace();
			}
		}
		
		plugin.logger.info(String.format("%s v%s has been enabled!", pdfFile.getName(), pdfFile.getVersion()));
	}
	
	
	@Override
	public void onDisable()
	{
		//Clear ArrayLists for reloads
		enabled_users.clear();
		staff.clear();
		RQ.clear();
		
		plugin.logger.info(String.format("%s v%s has been disabled!", pdfFile.getName(), pdfFile.getVersion()));
	}
    
}
