package net.HolioCraft.HolioTools.config;

import net.HolioCraft.HolioTools.main.Main;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

public class ConfigChecker
{
	private Main plugin;
	
	public ConfigChecker(Main plugin)
	{
		this.plugin = plugin;
	}
	
	/**
	 * Checks the config.yml on start up and reloads to ensure all components are present.
	 */
	public void checkConfig()
	{
		File file = new File(plugin.getDataFolder() + File.separator + "config.yml");
		plugin.saveDefaultConfig();
		if(!file.exists())
		{
			plugin.logger.info("Generating config.yml");
			plugin.getConfig().options().copyDefaults(true);
			plugin.saveConfig();
		}
		
		//If the version doesn't match, make backup of config
		if(plugin.getConfig().getString("Version") != null)
		{
			if(!plugin.getConfig().getString("Version").equals(plugin.pdfFile.getVersion()))
			{
				File fileBackup = new File(plugin.getDataFolder() + File.separator + "config.yml.backup");
				try
				{
					FileUtils.copyFile(file, fileBackup);
				}
				catch(IOException ex)
				{
					plugin.logger.info("Attempted to create a backup of config.yml, but failed.");
					plugin.logger.info("This is just informative. No need to take action.");
				}
				
				plugin.getConfig().set("Version", plugin.pdfFile.getVersion());
			}
		}
		
		
		
		//JoinSound
		if(plugin.getConfig().getString("JoinSound.Enabled") == null)
			plugin.getConfig().set("JoinSound.Enabled", true);
		if(plugin.getConfig().getString("JoinSound.Sound") == null)
			plugin.getConfig().set("JoinSound.Sound", "NOTE_PIANO");
		if(plugin.getConfig().getString("JoinSound.Pitch") == null)
			plugin.getConfig().set("JoinSound.Pitch", 1);
		//End JoinSound
		
		//DeathMessage
		if(plugin.getConfig().getString("DeathMessage.Enabled") == null)
			plugin.getConfig().set("DeathMessage.Enabled", true);
		if(plugin.getConfig().getString("DeathMessage.TNT") == null)
			plugin.getConfig().set("DeathMessage.TNT", "&e%player was blown to smithereens by some TNT!");
		if(plugin.getConfig().getString("DeathMessage.Drowning") == null)
			plugin.getConfig().set("DeathMessage.Drowning", "&e%player tripped into the kiddy pool.");
		if(plugin.getConfig().getString("DeathMessage.Cactus") == null)
			plugin.getConfig().set("DeathMessage.Cactus", "&e%player got naughty with a cactus. ;)");
		if(plugin.getConfig().getString("DeathMessage.Creeper") == null)
			plugin.getConfig().set("DeathMessage.Creeper", "&e%player died by the terrorists!");
		if(plugin.getConfig().getString("DeathMessage.Fall") == null)
			plugin.getConfig().set("DeathMessage.Fall", "&eHave a nice trip, %player see you next fall! &e%player fell %blocks blocks!");
		if(plugin.getConfig().getString("DeathMessage.FallingBlock") == null)
			plugin.getConfig().set("DeathMessage.FallingBlock", "&e%player some how got crushed by a block...");
		if(plugin.getConfig().getString("DeathMessage.Fire") == null)
			plugin.getConfig().set("DeathMessage.Fire", "&eHey, %player try standing further away from the fire.");
		if(plugin.getConfig().getString("DeathMessage.Ghast") == null)
			plugin.getConfig().set("DeathMessage.Ghast", "&eHey %player! Dem ghasts sure are killers!");
		if(plugin.getConfig().getString("DeathMessage.Magic") == null)
			plugin.getConfig().set("DeathMessage.Magic", "&e%player was killed by dat magic.");
		if(plugin.getConfig().getString("DeathMessage.Lava") == null)
			plugin.getConfig().set("DeathMessage.Lava", "&eDear %player, the lava thanks you for the items.");
		if(plugin.getConfig().getString("DeathMessage.Skeleton") == null)
			plugin.getConfig().set("DeathMessage.Skeleton", "&e%player was 1337 quick scop''d by a Skeleton.");
		if(plugin.getConfig().getString("DeathMessage.PlayerBow") == null)
			plugin.getConfig().set("DeathMessage.PlayerBow", "&e%player was 1337 quick scop''d by %killer.");
		if(plugin.getConfig().getString("DeathMessage.PlayerWeapon") == null)
			plugin.getConfig().set("DeathMessage.PlayerWeapon", "&e%player got wrecked by %killer.");
		if(plugin.getConfig().getString("DeathMessage.Hunger") == null)
			plugin.getConfig().set("DeathMessage.Hunger", "&e%player skipped lunch and starved to death...'");
		if(plugin.getConfig().getString("DeathMessage.Suffocation") == null)
			plugin.getConfig().set("DeathMessage.Suffocation", "&eOuch. %player met the wall in person.");
		if(plugin.getConfig().getString("DeathMessage.Void") == null)
			plugin.getConfig().set("DeathMessage.Void", "&e%player... How did you fall out of the world? o.o");
		if(plugin.getConfig().getString("DeathMessage.Mob") == null)
			plugin.getConfig().set("DeathMessage.Mob", "&e%player was mauled to death by a few mobs.");
		if(plugin.getConfig().getString("DeathMessage.Unknown") == null)
			plugin.getConfig().set("DeathMessage.Unknown", "&e%player died by unknown causes.");
		//End DeathMessage
		
		//RTP
		if(plugin.getConfig().getString("rtp.Radius") == null)
			plugin.getConfig().set("rtp.Radius", 2000);
		//End RTP
		
		//JoinMessages
		if(plugin.getConfig().getString("JoinMessages.Enabled") == null)
			plugin.getConfig().set("JoinMessages.Enabled", true);
		if(plugin.getConfig().getString("JoinMessages.ReturningPlayer") == null)
			plugin.getConfig().set("JoinMessages.ReturningPlayer", "&aWelcome %player back to the server!");
		if(plugin.getConfig().getString("JoinMessages.FirstJoin") == null)
			plugin.getConfig().set("JoinMessages.FirstJoin", "&dPlease welcome first time player: &a%player&d to the server!");
		//End JoinMessages
		
		//QuitMessage
		if(plugin.getConfig().getString("QuitMessage.Enabled") == null)
				plugin.getConfig().set("QuitMessage.Enabled", true);
		if(plugin.getConfig().getString("QuitMessage.Message") == null)
			plugin.getConfig().set("QuitMessage.Message", "&c%player has departed the server!");
		//End QuitMessage
		
		//RageQuit
		if(plugin.getConfig().getString("RageQuit.QuitMessage") == null)
			plugin.getConfig().set("RageQuit.QuitMessage", "&4%player HAS RAGE QUIT THE SERVER!!");
		if(plugin.getConfig().getString("RageQuit.JoinMessage") == null)
			plugin.getConfig().set("RageQuit.JoinMessage", "&aHopefully %player has cooled off from their rage...");
		//End RageQuit
		
		//PlayersPerIP
		if(plugin.getConfig().getString("PlayersPerIP.Enabled") == null)
			plugin.getConfig().set("PlayersPerIP.Enabled", false);
		if(plugin.getConfig().getString("PlayersPerIP.Number") == null)
			plugin.getConfig().set("PlayersPerIP.Number", 2);
		if(plugin.getConfig().getString("PlayersPerIP.KickMessage") == null)
			plugin.getConfig().set("PlayersPerIP.KickMessage", "Sorry, too many players on your IP! IP: %IP Max players: %max");
		if(plugin.getConfig().getString("PlayersPerIP.Whitelist") == null)
			plugin.getConfig().set("PlayersPerIP.Whitelist", "127.0.0.1, 192.168.1.1");
		//End PlayersPerIP
		
		//DuplicateIPAlert
		if(plugin.getConfig().getString("DuplicateIPAlert.Enabled") == null)
			plugin.getConfig().set("DuplicateIPAlert.Enabled", true);
		if(plugin.getConfig().getString("DuplicateIPAlert.MessageToStaff") == null)
			plugin.getConfig().set("DuplicateIPAlert.MessageToStaff", "%players have logged in on the same IP! (%IP)");
		if(plugin.getConfig().getString("DuplicateIPAlert.Whitelist") == null)
			plugin.getConfig().set("DuplicateIPAlert", "127.0.0.1, 192.168.1.1");
		//End DuplicateIPAlert
		
		//CapsCheck
		if(plugin.getConfig().getString("CapsCheck.Enabled") == null)
			plugin.getConfig().set("CapsCheck.Enabled", true);
		if(plugin.getConfig().getString("CapsCheck.LengthToCheck") == null)
			plugin.getConfig().set("CapsCheck.LengthToCheck", 7);
		if(plugin.getConfig().getString("CapsCheck.PercentCaps") == null)
			plugin.getConfig().set("CapsCheck.PercentCaps", .70);
		if(plugin.getConfig().getString("CapsCheck.Action") == null)
			plugin.getConfig().set("CapsCheck.Action", "lower");
		//End CapsCheck
		
		//Stats
		if(plugin.getConfig().getString("Stats") == null)
			plugin.getConfig().set("Stats", true);
		//End Stats
		
		//Update
		if(plugin.getConfig().getString("Update.Check") == null)
			plugin.getConfig().set("Update.Check", true);
		if(plugin.getConfig().getString("Update.Download") == null)
			plugin.getConfig().set("Update.Download", true);
		//Update to new format v1.2
		if(plugin.getConfig().getString("Download") != null)
			plugin.getConfig().set("Download", null);
		if(plugin.getConfig().getString("Check") != null)
			plugin.getConfig().set("Check", null);
		//End Update
		
		plugin.saveConfig();
	}
}
