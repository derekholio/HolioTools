package net.HolioCraft.HolioTools.events;

import net.HolioCraft.HolioTools.main.Main;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class PlayerChatListener implements Listener
{
	private Main plugin;
	
	public PlayerChatListener(Main plugin)
	{
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onChat(AsyncPlayerChatEvent event)
	{
		Player player = (Player)event.getPlayer();
		String message = event.getMessage();
		
		//Caps checking
		if(!player.hasPermission("holiotools.useCaps") && plugin.getConfig().getBoolean("CapsCheck.Enabled"))
		{
			double isCaps = 0;
			double isLower = 0;
			String CAPS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			String lower = "abcdefjhijklmnopqrstuvwxyz";
			
			if(message.length() >= plugin.getConfig().getInt("CapsCheck.LengthToCheck"))
			{
				for(int i = 0; i < message.length(); i++)
				{
					if(CAPS.contains(String.valueOf(message.charAt(i))))
					{
						isCaps++;
					}
					else if(lower.contains(String.valueOf(message.charAt(i))))
					{
						isLower++;
					}
					else
					{
						isLower++;
					}
				}
				if((isCaps/isLower) > plugin.getConfig().getDouble("CapsCheck.PercentCaps"))
				{
					if(plugin.getConfig().getString("CapsCheck.Action").equalsIgnoreCase("lower"))
					{
						message = message.toLowerCase();
						player.sendMessage(ChatColor.RED + "Too many caps! Message made lowercase.");
						event.setMessage(message);
					}
					else if(plugin.getConfig().getString("CapsCheck.Action").equalsIgnoreCase("cancel"))
					{
						player.sendMessage(ChatColor.RED + "Too many caps! Message has been canceled.");
						event.setCancelled(true);
					}
					
				}
				
			}
		}
		//End caps check
		
		//leetspeek
		if(plugin.enabled_users.contains(event.getPlayer()))
		{
			message = message.replace('a','4');
			message = message.replace('A', '4');
			message = message.replace('i', '1');
			message = message.replace('I', '1');
			message = message.replace('e','3');
			message = message.replace('E','3');
			message = message.replace('o','0');
			message = message.replace('O','0');
			message = message.replace('t', '7');
			message = message.replace('T', '7');
			event.setMessage(message);
		}
		//End leetspeak
		
		//Gray italic for messages in: *<MESSAGE>*
		if((message.charAt(0) == '*') && (message.charAt(message.length()-1) == '*'))
		{
			message = message.substring(1, message.length()-1);
			message = ChatColor.GRAY + "" + ChatColor.ITALIC + message;
			event.setMessage(message);
		}
		//End special format
	}
}
