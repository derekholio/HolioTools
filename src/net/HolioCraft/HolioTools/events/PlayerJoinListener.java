package net.HolioCraft.HolioTools.events;

import net.HolioCraft.HolioTools.main.Main;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener implements Listener 
{
	private Main plugin;
	
	public PlayerJoinListener(Main plugin)
	{
		this.plugin = plugin;
	}
	
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event)
    {
		Player player = event.getPlayer();
		Collection<? extends Player> players = Bukkit.getServer().getOnlinePlayers();
		//Player[] players = Bukkit.getServer().getOnlinePlayers();
		
		//Add players with staff permission to staff ArrayList
		if(player.hasPermission("holiotools.staff") || player.isOp())
    	{
    		plugin.staff.add(player);
    	}
    	//End staff add
		
		//Join messages
		if(plugin.getConfig().getBoolean("JoinMessages.Enabled")==true)
		{
			if(plugin.RQ.contains(player))
			{
				String message = plugin.getConfig().getString("RageQuit.JoinMessage");
				message = message.replace("%player", player.getName());
				event.setJoinMessage(ChatColor.translateAlternateColorCodes('&', message));
			}
			else
			{
				String joinMessage;
				if(player.hasPlayedBefore())
				{
					joinMessage = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("JoinMessages.ReturningPlayer"));
					if(joinMessage.contains("%player"))
					{
						joinMessage = joinMessage.replace("%player",player.getName());
					}
					event.setJoinMessage(joinMessage);
				}
				else if(!player.hasPlayedBefore())
				{
					joinMessage = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("JoinMessages.FirstJoin"));
					if(joinMessage.contains("%player"))
					{
						joinMessage = joinMessage.replace("%player",player.getName());
					}
					event.setJoinMessage(joinMessage);
				}
			}
		}
		//End join messages
		
		//Join sound
		if(plugin.getConfig().getBoolean("JoinSound.Enabled")==true)
		{
			Sound sound = Sound.valueOf(plugin.getConfig().getString("JoinSound.Sound"));
			try
			{
				for(Player e : players)
				{
					e.playSound(e.getLocation(), sound, 10, 1);
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
    	//End join sound
		
		//Duplicate IP checking
		if(plugin.getConfig().getBoolean("DuplicateIPAlert.Enabled"))
		{
			InetAddress IP = event.getPlayer().getAddress().getAddress();
			String IP1 = IP+"";
			IP1 = IP1.substring(1, IP1.length());
			//Bukkit.broadcastMessage(IP1);
			Collection<? extends Player> online = Bukkit.getServer().getOnlinePlayers();
	    	//Player[] online = Bukkit.getServer().getOnlinePlayers();
	    	List<String> dupeIPs = new ArrayList<String>();
	    	for(Player e: online)
	    	{
    			InetAddress IP2 = e.getPlayer().getAddress().getAddress();
    			String IP3 = IP2+"";
    			IP3 = IP3.substring(1, IP3.length());
    			String IPList = plugin.getConfig().getString("DuplicateIPAlert.Whitelist");
	    		if(IP1.equals(IP3) && (IPList == null || IPList.contains(IP3)==false))
	    		{
	    			dupeIPs.add(e.getName());
	    		}
	    	}
	    	//Bukkit.broadcastMessage("Dupe IP size: " + dupeIPs.size()+"");
	    	//Bukkit.broadcastMessage("Dupe IP: " + dupeIPs.toString());
	    	if(dupeIPs.size()>1)
	    	{
	    		String dupeIPPlayers = "";
	    		for(int i = 1; i < dupeIPs.size(); i++)
	    		{
	    			dupeIPPlayers = dupeIPs.get(i-1) + ",";
	    		}
	    		dupeIPPlayers = dupeIPPlayers + " and " + dupeIPs.get(dupeIPs.size()-1);
	    		String message = plugin.getConfig().getString("DuplicateIPAlert.MessageToStaff");
	    		for(int i = 0; i <= plugin.staff.size()-1; i++)
	    		{
	    			message = message.replace("%players", dupeIPs.toString().substring(1, dupeIPs.toString().length()-1));
	    			message = message.replace("%IP", IP1);
	    			plugin.staff.get(i).sendMessage(message);
	    		}
	    	}
		}
		//End duplicate IP checking
    }
}
