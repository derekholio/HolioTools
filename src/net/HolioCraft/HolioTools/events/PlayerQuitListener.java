package net.HolioCraft.HolioTools.events;

import net.HolioCraft.HolioTools.main.Main;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitListener implements Listener
{
	private Main plugin;
	
	public PlayerQuitListener(Main plugin)
	{
		this.plugin = plugin;
	}
	
    //QUIT MESSAGE
    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) 
    {
    	Player player = event.getPlayer();
    	if(plugin.RQ.contains(player))
    	{
    		event.setQuitMessage(ChatColor.RED + player.getName().toUpperCase() + ChatColor.RED + " HAS RAGE QUIT!");
    	}
    	else
    	{
			if(plugin.getConfig().getBoolean("QuitMessage.Enabled")==true)
			{
				
				plugin.enabled_users.remove(player);
				String quitMessage = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("QuitMessage.Message"));
				if(quitMessage.contains("%player"))
				{
					quitMessage = quitMessage.replace("%player",player.getName());
				}
				event.setQuitMessage(quitMessage);
			}
			
			if(event.getPlayer().hasPermission("holiotools.staff") || player.isOp())
			{
				plugin.staff.remove(player);
			}
    	}
    }
}
