package net.HolioCraft.HolioTools.events;

import net.HolioCraft.HolioTools.main.Main;

import java.net.InetAddress;
import java.util.Collection;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent.Result;

public class PlayerPreLoginListener implements Listener 
{
	private Main plugin;
	
	public PlayerPreLoginListener(Main plugin)
	{
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onPlayerLogin(AsyncPlayerPreLoginEvent event)
	{
		//Player[] players = Bukkit.getServer().getOnlinePlayers();
		InetAddress IP = event.getAddress();
		
		String IP1 = IP.toString();
		IP1 = IP1.substring(1, IP1.length());
		Collection<? extends Player> online = Bukkit.getServer().getOnlinePlayers();
    	//Player[] online = Bukkit.getServer().getOnlinePlayers();
    	int tmip = 1;
    	for(Player e: online)
    	{
    		if(!e.getPlayer().getName().equals(event.getName()))
    		{
    			InetAddress IP2 = e.getPlayer().getAddress().getAddress();
    			String IP3 = IP2+"";
    			IP3 = IP3.substring(1, IP3.length());
	    		if(IP1.equals(IP3) && plugin.getConfig().getBoolean("PlayersPerIP.Enabled")==true)
	    		{
	    			tmip++;
	    			
	    			String IPList = plugin.getConfig().getString("PlayersPerIP.Whitelist");
	    			//Bukkit.broadcastMessage(IPList);
	    			if((tmip > plugin.getConfig().getInt("PlayersPerIP.Number")) && IPList.contains(IP3)==false)
	    			{
	    				String kick = plugin.getConfig().getString("PlayersPerIP.KickMessage");
		    			kick = kick.replace("%IP", IP3);
		    			kick = kick.replace("%max", plugin.getConfig().getInt("PlayersPerIP.Number")+"");
		    			kick = ChatColor.translateAlternateColorCodes('&', kick);
	    				event.disallow(Result.KICK_OTHER, kick);
	    			}
	    		}
	    		else
	    		{
	    			event.allow();
	    		}
    		}
    	}
		
	}

}
