package net.HolioCraft.HolioTools.events;

import net.HolioCraft.HolioTools.main.Main;

import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PlayerDeathEvent;

public class PlayerDeathListener implements Listener
{
	private Main plugin;
	
	public PlayerDeathListener(Main plugin)
	{
		this.plugin = plugin;
	}
	
		//CUSTOM DEATH MESSAGES
		@EventHandler(priority = EventPriority.MONITOR)
	    public void onDeathOfPlayer(PlayerDeathEvent en)
	    {
	    	if(plugin.getConfig().getBoolean("DeathMessage.Enabled")==true)
			{
				Entity ent;
				DamageCause dc;
				EntityDamageEvent ede;
				String deathMessage = "";
				
				ent = en.getEntity();
				ede = ent.getLastDamageCause();
				try
				{
					dc = ede.getCause();
				}
				catch (Exception E)
				{
					dc = null;
				}
				
				if(ent instanceof Player && dc == DamageCause.BLOCK_EXPLOSION) 
				{
					deathMessage = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("DeathMessage.TNT"));
					Player p = (Player)ent;
					deathMessage = deathMessage.replace("%player",p.getName());
					en.setDeathMessage(deathMessage);
				}
				
				else if(ent instanceof Player && dc == DamageCause.DROWNING)
				{
					deathMessage = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("DeathMessage.Drowning"));
					Player p = (Player)ent;
					deathMessage = deathMessage.replace("%player",p.getName());
					en.setDeathMessage(deathMessage);
				}
				
				else if(ent instanceof Player && dc == DamageCause.CONTACT)
				{
					deathMessage = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("DeathMessage.Cactus"));
					Player p = (Player)ent;
					deathMessage = deathMessage.replace("%player",p.getName());
					en.setDeathMessage(deathMessage);
				}
				
				else if(ent instanceof Player && dc == DamageCause.ENTITY_EXPLOSION)
				{
					deathMessage = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("DeathMessage.Creeper"));
					Player p = (Player)ent;
					deathMessage = deathMessage.replace("%player",p.getName());
					en.setDeathMessage(deathMessage);        	
				}
				
				else if(ent instanceof Player && dc == DamageCause.FALL)
				{
					deathMessage = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("DeathMessage.Fall"));
					Player p = (Player)ent;
					deathMessage = deathMessage.replace("%player",p.getName());
					int fallHeight = (int)(p.getFallDistance());
					deathMessage = deathMessage.replace("%blocks",fallHeight+"");
					en.setDeathMessage(deathMessage);
				}
				
				else if(ent instanceof Player && dc == DamageCause.FALLING_BLOCK)
				{
					deathMessage = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("DeathMessage.FallingBlock"));
					Player p = (Player)ent;
					deathMessage = deathMessage.replace("%player",p.getName());
					en.setDeathMessage(deathMessage);
				}
				
				else if(ent instanceof Player && dc == DamageCause.FIRE || ent instanceof Player && dc == DamageCause.FIRE_TICK)
				{
					deathMessage = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("DeathMessage.Fire"));
					Player p = (Player)ent;
					deathMessage = deathMessage.replace("%player",p.getName());
					en.setDeathMessage(deathMessage);
				}
				
				else if(ent instanceof Player && dc == DamageCause.MAGIC)
				{
					deathMessage = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("DeathMessage.Magic"));
					Player p = (Player)ent;
					deathMessage = deathMessage.replace("%player",p.getName());
					en.setDeathMessage(deathMessage);
				}
				
				else if(ent instanceof Player && dc == DamageCause.LAVA)
				{
					deathMessage = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("DeathMessage.Lava"));
					Player p = (Player)ent;
					deathMessage = deathMessage.replace("%player",p.getName());
					en.setDeathMessage(deathMessage);
				}
				
				else if(ent instanceof Player && dc == DamageCause.PROJECTILE)
				{
					Player p = (Player)ent;
					if(p.getKiller()==null)
					{
						deathMessage = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("DeathMessage.Skeleton"));
						deathMessage = deathMessage.replace("%player",p.getName());
						en.setDeathMessage(deathMessage);
					}
					else
					{
						deathMessage = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("DeathMessage.PlayerBow"));
						deathMessage = deathMessage.replace("%player",p.getName());
						deathMessage = deathMessage.replace("%killer",p.getKiller().getName());
						en.setDeathMessage(deathMessage);
					}
				}
				
				else if(ent instanceof Player && dc == DamageCause.STARVATION)
				{
					deathMessage = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("DeathMessage.Hunger"));
					Player p = (Player)ent;
					deathMessage = deathMessage.replace("%player",p.getName());
					en.setDeathMessage(deathMessage);
				}
				
				else if(ent instanceof Player && dc == DamageCause.SUFFOCATION)
				{
					deathMessage = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("DeathMessage.Suffocation"));
					Player p = (Player)ent;
					deathMessage = deathMessage.replace("%player",p.getName());
					en.setDeathMessage(deathMessage);
				}
				
				else if(ent instanceof Player && dc == DamageCause.VOID)
				{
					deathMessage = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("DeathMessage.Void"));
					Player p = (Player)ent;
					deathMessage = deathMessage.replace("%player",p.getName());
					en.setDeathMessage(deathMessage);
				}
				
				else if(ent instanceof Player && dc == DamageCause.ENTITY_ATTACK)
				{
					if(en.getEntity().getKiller() != null)
					{
						if(en.getEntity() instanceof Player)
						{
							deathMessage = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("DeathMessage.PlayerWeapon"));
							Player p = (Player)ent;
							deathMessage = deathMessage.replace("%player",p.getName());
							deathMessage = deathMessage.replace("%killer",p.getKiller().getName());
							en.setDeathMessage(deathMessage);                  
						}
					}
					else
					{	
						deathMessage = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("DeathMessage.Mob"));
						Player p = (Player)ent;
						deathMessage = deathMessage.replace("%player",p.getName());
						en.setDeathMessage(deathMessage);
					}
					
				}
				
				else if(!(ent instanceof Player))
				{
					//Error catching. 
				}
				
				else if(ent instanceof Player && dc == null)
				{
					deathMessage = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("DeathMessage.Unknown"));
					Player p = (Player)ent;
					deathMessage = deathMessage.replace("%player",p.getName());
					en.setDeathMessage(deathMessage);
				}
			}
		}
		//END CUSTOM MESSAGES
}
