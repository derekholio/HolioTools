package net.HolioCraft.HolioTools.commands;

import net.HolioCraft.HolioTools.main.Main;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class UUID implements CommandExecutor 
{
	@SuppressWarnings("unused")
	private Main plugin;

	public UUID(Main plugin)
	{
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if(sender.hasPermission("HolioTools.UUID"))
		{
			if(args.length > 0)
			{
				Player p = Bukkit.getPlayer(args[0]);
				if(p == null)//If the player is offline
				{
					OfflinePlayer op = Bukkit.getOfflinePlayer(args[0]);
					if(op.hasPlayedBefore())
					{						
						sender.sendMessage(String.format(ChatColor.GREEN + "Here is %s's UUID: ", args[0]));
						sender.sendMessage(op.getUniqueId().toString());
						sender.sendMessage(ChatColor.DARK_RED + "This could be inaccurate, since names can change!");
						return true;
					}
					else
					{
						if(args.length > 1 && args[1].equalsIgnoreCase("force"))
						{							
							sender.sendMessage(String.format(ChatColor.DARK_RED + "It doesn't look like %s has played here before.", args[0]));
							sender.sendMessage(String.format(ChatColor.GREEN + "This is what I got for %s's UUID: ", args[0]));
							sender.sendMessage(op.getUniqueId().toString());
							sender.sendMessage(ChatColor.DARK_RED + "This could be inaccurate, since names can change!");
						}
						else
						{
							sender.sendMessage(ChatColor.DARK_RED + "It doesn't look like " + args[0] + " has played here.");
							sender.sendMessage(ChatColor.DARK_RED + "We can still attempt to get a UUID, but this has a small chance to lag/crash the server.");
							sender.sendMessage(ChatColor.DARK_RED + "If you wish to continue, run /UUID " + args[0] + " force");
						}
						return true;
					}
				}
				else//If the player is online
				{
					java.util.UUID OnlineUUID = p.getUniqueId();
					sender.sendMessage(args[0] + "'s UUID is: ");
					sender.sendMessage(OnlineUUID.toString());
					return true;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to do that!");
			return false;
		}
	}
}
