package net.HolioCraft.HolioTools.commands;

import net.HolioCraft.HolioTools.main.Main;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Leetspeak implements CommandExecutor 
{
	private Main plugin;

	public Leetspeak(Main plugin)
	{
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		//Enable/disable 1337Speak
		if(sender instanceof Player)
		{
			if(args.length == 0)
			{
				Player player = (Player)sender;
				if(plugin.enabled_users.contains(player))
				{
					plugin.enabled_users.remove(player);
					player.sendMessage("Leet speak has been disabled.");
				}
				else
				{
					plugin.enabled_users.add(player);
					player.sendMessage(String.format("Leet speak enabled. Type /%s to disable.", commandLabel.toLowerCase()));
				}
				return true;
			}
			
			if(args.length == 1)
			{
				if(sender.hasPermission("holiotools.leetspeak.other"))
				{
					Player target = Bukkit.getServer().getPlayer(args[0]);
					if(target == null)
					{
						sender.sendMessage(args[0] + " is not online...");
					}
					else if(plugin.enabled_users.contains(target))
					{
						plugin.enabled_users.remove(target);
						sender.sendMessage(String.format("Leet speak has been disabled for %s", target.getName()));
						target.sendMessage(String.format("Leet speak has been disabled by %s", sender.getName()));
					}
					else
					{
						plugin.enabled_users.add(target);
						sender.sendMessage(String.format("Leet speak has been enabled for %s", target.getName()));
						target.sendMessage(String.format("Leet speak has been enabled by %s", sender.getName()));
					}
				}
				else
				{
					sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to do that!");
				}
				return true;	
			}
			
			else
			{
				return false;
			}
		}
		else
		{
			sender.sendMessage(ChatColor.RED + "Sorry console, but you can't have " + commandLabel);
			return true;
		}
	}
	
}