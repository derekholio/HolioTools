package net.HolioCraft.HolioTools.commands;

import net.HolioCraft.HolioTools.main.Main;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;

public class Clearall implements CommandExecutor 
{
	@SuppressWarnings("unused")
	private Main plugin;

	public Clearall(Main plugin)
	{
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if(sender instanceof ConsoleCommandSender && args.length==0)
		{
			sender.sendMessage(ChatColor.RED + "Ugh... Having issues clearing the console inventory.");
			return true;
		}
		else
		{
			Player player = (Player)sender;
			
			if(args.length == 0)
			{
				PlayerInventory inv = player.getInventory();
				//Remove all items in inventory
				inv.clear();
				//Remove all armor
				inv.setLeggings(null);
				inv.setHelmet(null);
				inv.setBoots(null);
				inv.setChestplate(null);
				sender.sendMessage(ChatColor.GREEN + "Cleared inventory and removed armor.");
				return true;
			}
			
			if(args.length == 1 && player.hasPermission("holiotools.clearall.others"))
			{
				Player target = Bukkit.getServer().getPlayer(args[0]);
				if(target == null)
				{
					sender.sendMessage(args[0] + " is not online...");
					return true;
				}
				else
				{
					PlayerInventory inv = target.getInventory();
					//Remove all items in inventory
					inv.clear();
					//Remove all armor
					inv.setLeggings(null);
					inv.setHelmet(null);
					inv.setBoots(null);
					inv.setChestplate(null);
					
					sender.sendMessage(String.format(ChatColor.GREEN + "Cleared %s's inventory, and removed all armor.", target.getName()));
					target.sendMessage(String.format(ChatColor.RED + "Inventory and armor removed by %s", sender.getName()));
					
					return true;
				}
			}
			else
			{
				return false;
			}
		}
	}
}