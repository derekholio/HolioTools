package net.HolioCraft.HolioTools.commands;

import net.HolioCraft.HolioTools.main.Main;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class Rtp implements CommandExecutor 
{
	private Main plugin;

	public Rtp(Main plugin)
	{
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if(sender instanceof ConsoleCommandSender)
		{
			sender.sendMessage(ChatColor.RED + "Sorry Console, no teleporting for you.");
			return true;
		}
		else
		{
			Player player = (Player)sender;
			int radius = plugin.getConfig().getInt("rtp.Radius");
			
			Location spawn = player.getWorld().getSpawnLocation();
			int x = (int) (Math.random() * (((spawn.getX()+radius) - (spawn.getX()-radius) + 1)));
			int y = (int) (Math.random()*256);
			int z = (int) (Math.random() * (((spawn.getZ()+radius) - (spawn.getZ()-radius) + 1)));
			Location newLoc = new Location(player.getWorld(), x, y, z);
			Block b = newLoc.getBlock();
			
			while(b.getType() != Material.AIR)//Up
			{
				newLoc = new Location(player.getWorld(), x, newLoc.getY()+1, z);
				b = newLoc.getBlock();
			}
			
			while(b.getType() == Material.AIR)//Down
			{
				newLoc = new Location(player.getWorld(), x, newLoc.getY()-1, z);
				b = newLoc.getBlock();
			}
			newLoc = new Location(player.getWorld(), newLoc.getX(), newLoc.getY()+2, newLoc.getZ());
			player.teleport(newLoc);
			player.sendMessage(String.format("Your new coordinates are: X: %s Y: %s Z: %s", newLoc.getX(), newLoc.getY(), newLoc.getZ()));
			
			return true;
		}
	}
	
}