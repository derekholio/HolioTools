package net.HolioCraft.HolioTools.commands;

import net.HolioCraft.HolioTools.main.Main;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class MakeOP implements CommandExecutor 
{
	@SuppressWarnings("unused")
	private Main plugin;

	public MakeOP(Main plugin)
	{
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if(sender instanceof ConsoleCommandSender)
		{
			sender.sendMessage(ChatColor.RED + "You are more than OP to me...");
			return true;
		}
		
		else
		{
			Player player = (Player)sender;
			if(!player.isOp())
				player.kickPlayer(ChatColor.RED + "No, you can't be OP!!");
			else
				player.sendMessage(ChatColor.GREEN + "But you are already OP...");
		}
		return true;
	}
	
}