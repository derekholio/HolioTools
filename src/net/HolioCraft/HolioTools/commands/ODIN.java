package net.HolioCraft.HolioTools.commands;

import net.HolioCraft.HolioTools.main.Main;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ODIN implements CommandExecutor 
{
	@SuppressWarnings("unused")
	private Main plugin;

	public ODIN(Main plugin)
	{
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if(args.length == 2)
		{
			Player target = Bukkit.getServer().getPlayer(args[0]);
			if(target == null)
			{
				sender.sendMessage(String.format(ChatColor.RED + "%s is offline.", args[0]));
				return true;
			}
			else
			{
				target.getWorld().createExplosion(target.getLocation(), Integer.parseInt(args[1]));
				sender.sendMessage(String.format(ChatColor.GREEN + "Blowing up %s", args[0]));
				return true;
			}
				
		}
		else if(args.length == 1)
		{
			Player target = Bukkit.getServer().getPlayer(args[0]);
			if(target == null)
			{
				sender.sendMessage(String.format(ChatColor.RED + "%s is offline.", args[0]));
				return true;
			}
			else
			{
				target.getWorld().createExplosion(target.getLocation(), 10);
				sender.sendMessage(String.format(ChatColor.GREEN + "Blowing up %s", args[0]));
				return true;
			}
				
		}
		else
		{
			return false;
		}
	}
	//End ODIN
}
