package net.HolioCraft.HolioTools.commands;

import net.HolioCraft.HolioTools.main.Main;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class MakeStaff implements CommandExecutor 
{
	@SuppressWarnings("unused")
	private Main plugin;

	public MakeStaff(Main plugin)
	{
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if(sender instanceof ConsoleCommandSender)
		{
			sender.sendMessage(ChatColor.RED + "Oh console... You are already more than staff.");
		}
		else
		{
			Player player = (Player)sender;
			if(!player.hasPermission("holiotools.staff"))
				player.kickPlayer(ChatColor.RED + "No, you can't be staff!!");
			else
				player.sendMessage(ChatColor.GREEN + "But wait... You are already staff... So we won't kick you.");
		}
		
		return true;
	}
	
}