package net.HolioCraft.HolioTools.commands;

import net.HolioCraft.HolioTools.main.Main;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class Spank implements CommandExecutor 
{
	@SuppressWarnings("unused")
	private Main plugin;

	public Spank(Main plugin)
	{
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
	    if((args.length < 1) || (args.length > 2))
	    {
	      return false;
	    }
		
	    else if(Bukkit.getPlayer(args[0]) == null)
	    {
	    	sender.sendMessage(String.format(ChatColor.RED + "Sorry, %s is offline.", args[0]));
			return true;
	    }
		
		else
		{
			Player target = Bukkit.getPlayer(args[0]);
		    int force = 5;
		    if (args.length == 2)
		    {
		      force = Integer.parseInt(args[1]);
		    }
		    Location targetLoc = target.getLocation();
		    World targetWorld = target.getWorld();
		    Vector v = targetLoc.getDirection().multiply(-1*force);
		    target.setVelocity(v);
		    try
		    {
		    	targetWorld.playSound(targetLoc, Sound.ZOMBIE_WOODBREAK, 1, 1);
		    }
		    catch(Exception E)
		    {
		    	targetWorld.playEffect(targetLoc, Effect.ZOMBIE_DESTROY_DOOR, 0);
		    }
		    sender.sendMessage(String.format(ChatColor.GREEN + "Spanked %s!", target.getName()));
		    target.sendMessage(String.format(ChatColor.GREEN + "You have been spanked by %s!", sender.getName()));
	        return true;
	    }
	}
	
}
