package net.HolioCraft.HolioTools.commands;

import net.HolioCraft.HolioTools.main.Main;
import net.HolioCraft.HolioTools.config.ConfigChecker;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.PluginDescriptionFile;

public class HolioTools implements CommandExecutor 
{
	private Main plugin;

	public HolioTools(Main plugin)
	{
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		ConfigChecker cc = new ConfigChecker(plugin);
		cc.checkConfig();		
		
		PluginDescriptionFile pdfFile = plugin.getDescription();
		sender.sendMessage(String.format("HolioTools v%s reloaded.", pdfFile.getVersion()));
		return true;
	}
	
}