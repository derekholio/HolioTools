package net.HolioCraft.HolioTools.commands;

import net.HolioCraft.HolioTools.main.Main;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Down implements CommandExecutor 
{
	@SuppressWarnings("unused")
	private Main plugin;

	public Down(Main plugin)
	{
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		//DOWN
		Player player = (Player)sender;
		if(args.length==1)
		{
			int blocks = Integer.parseInt(args[0]);
			if(blocks < 0)
			{
				sender.sendMessage(ChatColor.RED + "The number of blocks must be positive!");
				return true;
			}
			else
			{
				Location location = player.getLocation();
				int x = (int) location.getX();
				int firstY = (int)location.getY();
				int y = (int)location.getY()-blocks;
				int z = (int)location.getZ();
				Location newLoc = new Location(player.getWorld(), x, y, z);
				Block b = newLoc.getBlock();
				
				while(b.getType() != Material.AIR)
				{
					newLoc = new Location(player.getWorld(), x, newLoc.getY()+1, z);
					b = newLoc.getBlock();
				}
				
				
				if(firstY == newLoc.getY())
				{
					player.sendMessage(ChatColor.RED + "Can't go any lower!");
				}
				else if((firstY-blocks) == (newLoc.getY()))
				{
					player.sendMessage(String.format(ChatColor.GREEN + "Moved down %d blocks.", blocks));
				}
				else
				{
					player.sendMessage(String.format(ChatColor.RED + "Only moved down %d blocks (So you don't get stuck).", (int)(firstY - newLoc.getY())));
				}
				
				player.teleport(newLoc);
				return true;
			}
			
		}
		else
		{
			return false;
		}
	}
}
