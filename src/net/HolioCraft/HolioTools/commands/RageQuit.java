package net.HolioCraft.HolioTools.commands;

import net.HolioCraft.HolioTools.main.Main;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class RageQuit implements CommandExecutor 
{
	private Main plugin;

	public RageQuit(Main plugin)
	{
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if(sender instanceof ConsoleCommandSender)
		{
			sender.sendMessage(ChatColor.GREEN + "Silly console... You can't rage quit! You have to run the server!");
		}
		else
		{
			Player p = (Player)sender;
			if(args.length == 0)
			{
				plugin.RQ.add(p);
				String message = plugin.getConfig().getString("RageQuit.QuitMessage");
				message = message.replace("%player", sender.getName());
				p.kickPlayer(ChatColor.translateAlternateColorCodes('&', message));
				return true;
			}
			else
			{
				return false;
			}
		}
		return true;
		
	}
	
}