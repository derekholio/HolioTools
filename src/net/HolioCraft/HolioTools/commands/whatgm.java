package net.HolioCraft.HolioTools.commands;

import net.HolioCraft.HolioTools.main.Main;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class whatgm implements CommandExecutor 
{
	@SuppressWarnings("unused")
	private Main plugin;

	public whatgm(Main plugin)
	{
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		if(args.length == 0)
		{
			if(sender instanceof ConsoleCommandSender)
			{
				sender.sendMessage(ChatColor.RED + "Hmm... I often wonder what gamemode the console is in...");
			}	
			else
			{
				if(Bukkit.getServer().getPlayer(sender.getName())==null)
				{
					sender.sendMessage(String.format(ChatColor.RED + "Sorry, %s is not online.", args[0]));
				}
				else
				{
					sender.sendMessage(String.format("Your gamemode is: %s", ((Player)sender).getGameMode().toString()));
				}
			}
			return true;
		}
		else if(args.length == 1)
		{
			Player target = Bukkit.getServer().getPlayer(args[0]);
			
			sender.sendMessage(String.format("%s is in gamemode: %s", target.getName(), target.getGameMode().toString()));
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
}